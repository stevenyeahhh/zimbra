package com.choucair.zimbra.automatizacion1.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MensajePageObject extends PageObject  {
	
	@FindBy(id="zv__COMPOSE1_to_control")
	private WebElementFacade txtDestinatario;
	@FindBy(id="zv__COMPOSE1_subject_control")
	private WebElementFacade txtAsunto;
	@FindBy(css=".ZmHtmlEditor textarea")
	private WebElementFacade txtCuerpo;
	
	@FindBy(id="zb__COMPOSE1__SEND")
	private WebElementFacade btnEnviar;
	
	
	public WebElementFacade getTxtDestinatario() {
		return txtDestinatario;
	}
	public void setTxtDestinatario(String txtDestinatario) {
		this.txtDestinatario.click();
		this.txtDestinatario.sendKeys(txtDestinatario);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public WebElementFacade getTxtAsunto() {
		return txtAsunto;
	}
	public void setTxtAsunto(String txtAsunto) {
		this.txtAsunto.click();
		this.txtAsunto.sendKeys(txtAsunto);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public WebElementFacade getTxtCuerpo() {
		return txtCuerpo;
	}
	public void setTxtCuerpo(String txtCuerpo) {
		this.txtCuerpo.click();
		this.txtCuerpo.sendKeys(txtCuerpo);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void enviar() {
		// TODO Auto-generated method stub
		btnEnviar.click();
	}
	
	
	
}
