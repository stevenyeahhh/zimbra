package com.choucair.zimbra.automatizacion1.definitions;

import com.choucair.zimbra.automatizacion1.steps.LoginStep;


import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginDefinition {
	@Steps
	private LoginStep loginStep;

	@Given("^ingresar a zimbra$")
	public void ingresarZimbra() {
		loginStep.ingresarZimbra();
	}
	
	@Given("^iniciar sesión iniciada con \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void iniciarSesion(String usuario, String contrasena) {
		
		loginStep.iniciarSesion(usuario,contrasena);
		
	}
}
