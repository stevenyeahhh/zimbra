package com.choucair.zimbra.automatizacion1.definitions;

import com.choucair.zimbra.automatizacion1.steps.MensajeStep;
import com.choucair.zimbra.automatizacion1.steps.MenuStep;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MensajeDefinition {
	@Steps
	private MensajeStep mensajeStep;
	
	@Steps
	private MenuStep menuStep;
	
	@When("^completar el asunto \"([^\"]*)\", cuerpo \"([^\"]*)\", y a \"([^\"]*)\"$") 
	public void redactar(String asunto,String cuerpo, String destinatario) {
		mensajeStep.redactar(asunto,cuerpo,destinatario);
	}
	@When("^enviar mensaje$")
	public void enviarMensaje() {
		mensajeStep.enviar();
	}
	@Then("^validar el envío del mensaje, con el asunto \"([^\"]*)\", cuerpo \"([^\"]*)\", y a \"([^\"]*)\"$")
	public void validarMensajeEnviado(String asunto,String cuerpo,String destinatario) {
		menuStep.validarMensajeEnviado(asunto,cuerpo,destinatario);
		
		
	}
	
}
