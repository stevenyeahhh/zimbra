package com.choucair.zimbra.automatizacion1.steps;




import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;


import net.thucydides.core.annotations.Step;

import com.choucair.zimbra.automatizacion1.pageobjects.LoginPageObject;




public class LoginStep {

	private LoginPageObject loginPageObject;
	
	@Step
	public void ingresarZimbra() {
		// TODO Auto-generated method stub
		loginPageObject.open();
	}
	@Step
	public void iniciarSesion(String usuario, String contrasena) {
		// TODO Auto-generated method stub
		loginPageObject.setTxtUsuario(usuario);
		loginPageObject.setTxtContrasena(contrasena);
		loginPageObject.submitBtn();
		loginPageObject.verificarCarga();
	}

}
