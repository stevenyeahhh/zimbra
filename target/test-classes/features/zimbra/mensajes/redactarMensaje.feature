@redactarMensaje
Feature: redactar mensaje
  
  @rutaCritica
  Scenario: redactar correo y validar el envío
    Given ingresar a zimbra
    And iniciar sesión iniciada con "jcaperap@choucairtesting.com" y contraseña "Bogo789*"
    When ingresar al módulo de mensajes
    And completar el asunto "prueba 1", cuerpo "Esto es un mensaje", y a "jcaperap@choucairtesting.com" 
    And enviar mensaje
    Then validar el envío del mensaje, con el asunto "prueba 1", cuerpo "Esto es un mensaje", y a "jcaperap@choucairtesting.com"

