package com.choucair.zimbra.automatizacion1.definitions;

import com.choucair.zimbra.automatizacion1.pageobjects.MenuPageObject;

import cucumber.api.java.en.When;

public class MenuDefinition {
	
	private MenuPageObject menuPageObject;
	@When("^ingresar al módulo de mensajes$") 
	public void ingresarMensajes() {
		System.out.println("..................");
//		menuPageObject.open();
		menuPageObject.presionarNuevoMensaje();
		menuPageObject.verificarModuloCargo();
	}

}
