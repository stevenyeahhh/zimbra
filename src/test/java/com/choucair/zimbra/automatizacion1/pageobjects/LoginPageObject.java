package com.choucair.zimbra.automatizacion1.pageobjects;



import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://mx1.choucairtesting.com")
public class LoginPageObject extends PageObject{
	@FindBy(id="username")
	private WebElementFacade txtUsuario;
	
	@FindBy(id="password")
	private WebElementFacade txtContrasena;

	@FindBy(css=".zLoginButton")
	private WebElementFacade  btnSubmit;
	
	@FindBy(css="title")
	private WebElementFacade  titPage;

	
	public WebElementFacade getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(String txtUsuario) {
		this.txtUsuario.click();
		this.txtUsuario.sendKeys(txtUsuario);
	}

	public WebElementFacade getTxtContrasena() {
		return txtContrasena;
	}

	public void setTxtContrasena(String txtContrasena) {
		this.txtContrasena.click();
		this.txtContrasena.sendKeys(txtContrasena);
	}
	
	public void submitBtn() {
		this.btnSubmit.click();
	}
		
	
	public void verificarCarga() {
		
		assertThat(getTitle(),containsString("Zimbra: Bandeja de entrada"));
		
	}
	
	
}
