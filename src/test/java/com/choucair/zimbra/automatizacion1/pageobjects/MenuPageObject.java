package com.choucair.zimbra.automatizacion1.pageobjects;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MenuPageObject extends PageObject {

	@FindBy(id="zb__CLV__NEW_MENU_left_icon")
	private WebElementFacade btnNuevoMensaje;
	
	@FindBy(css="#ztb_appChooser_items td")
	private List<WebElementFacade> mnToolbar;
	
	
	
	@FindBy(id="zti__main_Mail__5")
	private WebElementFacade btnEnviados;
	//@FindBy(css="#zl__CLV__rows div .RowEven")
	//private WebElementFacade opElementoReciente;
	
	
	@FindBy(css="#zl__CLV__rows .Row:first-child td:nth-child(7)")
	private WebElementFacade lblCorreo;

	@FindBy(css="#zv__CLV__MSG_hdrTable>tbody tr:nth-child(2) td:nth-child(2)")
	private WebElementFacade lblDestinatario;
	
	@FindBy(css=".SubjectCol")
	private WebElementFacade lblAsunto;
	
	@FindBy(css=".MsgBody")
	private WebElementFacade lblCuerpo;
	
	
	@FindBy(id="zi_search_inputfield")
	private WebElementFacade txtBuscar;

	
	@FindBy(id="zb__Search__SEARCH_title")
	private WebElementFacade btnBuscar;
	
	public void presionarNuevoMensaje() {
		// TODO Auto-generated method stub
		System.out.println("hola...");
		System.out.println(mnToolbar.size());
		btnNuevoMensaje.click();
		System.out.println(mnToolbar.size());
		System.out.println("hola2...");
	}
	public void verificarModuloCargo() {
		// TODO Auto-generated method stub
		
		assertThat(mnToolbar.size(), is(26));
	}
	public void presionarEnviados() {
		// TODO Auto-generated method stub
		
		
		btnEnviados.click();
	}
	public void verificarUltimoMensaje(String asunto, String cuerpo, String destinatario) {
		
		assertThat(lblAsunto.getText(),containsString(asunto));
		
		assertThat(lblDestinatario.getText(),is(destinatario));
		getDriver().switchTo().frame(getDriver().findElement(By.id("zv__CLV__MSG_body__iframe")));

		
		assertThat(getDriver().findElement(By.cssSelector(".MsgBody")).getText(),containsString(cuerpo));
		
	}
	public void buscarAsunto(String asunto) {
		// TODO Auto-generated method stub
		txtBuscar.click();
		txtBuscar.sendKeys(asunto);
		btnBuscar.click();
	}
	public void seleccionarUltimoCorreoEnviado() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lblCorreo.click();
	}

}
