package com.choucair.zimbra.automatizacion1.steps;

import com.choucair.zimbra.automatizacion1.pageobjects.MenuPageObject;

public class MenuStep {
	private MenuPageObject menuPageObject;
	public void validarMensajeEnviado(String asunto,String cuerpo,String destinatario) {
		// TODO Auto-generated method stub
		menuPageObject.presionarEnviados();
		menuPageObject.buscarAsunto(asunto);
		menuPageObject.seleccionarUltimoCorreoEnviado();
		menuPageObject.verificarUltimoMensaje(asunto,cuerpo,destinatario);
	}
}
